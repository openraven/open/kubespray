#! /usr/bin/env bash
set -euo pipefail
if [[ -z "${AUTO_START:-}" ]]; then
  exec sed -e "s/[$]RELEASE_ID/$RELEASE_ID/" bootstrap.sh
fi
# kick things off; it's easier than trying to tease apart the ansible-runner
# from its enclosing httpd
bash -c 'sleep 5; curl -sfo event-source http://127.0.0.1:8000/event-source' &
disown
exec ./scripts/install_httpd.py "$@"
