#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function
__metaclass__ = type
try:
    import boto3
    from botocore.exceptions import ClientError
    HAS_BOTO3 = True
except ImportError:
    HAS_BOTO3 = False

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.ec2 import (boto3_conn, camel_dict_to_snake_dict,
                                      ec2_argument_spec, get_aws_connection_info)


ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = """
---
module: orvn_ec2_instance_type_facts
short_description: Discover available instance types by criteria
description:
  - Looks up the available instance types for the provided criteria,
    optionally union-ing them to find set of types that will work across
    all specified availability zones.
  - None of the module arguments are required, but the arguments
    ``availability_zones:``, ``availability_zone_ids``, and ``regions:``
    are mutually exclusive of one another
version_added: "2.0"
options:
    availability_zones:
      required: false
      default: ~
      description:
      - a list of the availability zones to use
    availability_zone_ids:
      required: false
      default: ~
      description:
      - a list of the availability zones IDs to use
    regions:
      required: false
      default: ~
      description:
      - a list of the regions to use
    all_zone_ids:
      required: false
      type: bool
      default: no
      description:
      - if true, runs as if ``availability_zone_ids`` was set to the list of
        all availability zone IDs for the current ``region:``
    only_common_types:
      required: false
      type: bool
      default: no
      description:
      - if true, return a fact containing only the common instance types
        across all provided filter values
requirements:
- botocore
- boto3
"""


def get_az_id_list(connection):
    """
    :type connection: botocore.client.BaseClient
    :rtype: list[dict[str, str]]
    """
    az_resp = connection.describe_availability_zones()
    only_keys = ['ZoneId', 'ZoneName']

    def only_keys_snaked(the_dict):
        result = {k: v for k, v in the_dict.items() if k in only_keys}
        return camel_dict_to_snake_dict(result)

    return [only_keys_snaked(az)
            for az in az_resp['AvailabilityZones']
            if az['State'] == 'available']


def get_instance_types_by_filter(connection, location_type, filters):
    """
    :type connection: botocore.client.BaseClient
    :type location_type: str
    :type filters: list[dict[str, list[str]]]
    :rtype: dict[str, set[str]]
    """
    ito_list = []
    cmd_kwargs = {
        'LocationType': location_type,
        'Filters': filters,
    }
    while True:
        # connection.get_paginator() throws as of botocore 1.13.28
        resp = connection.describe_instance_type_offerings(**cmd_kwargs)
        ito_list += resp['InstanceTypeOfferings']
        token = resp.get('NextToken')
        if not token:
            break
        cmd_kwargs['NextToken'] = token
    i_types_by_location = {}
    for ito in ito_list:
        location = ito['Location']
        i_type = ito['InstanceType']
        location_types = i_types_by_location.setdefault(location, set())
        location_types.add(i_type)
    return i_types_by_location


def main():
    argument_spec = ec2_argument_spec()
    main_spec = dict(
        regions=dict(default=[], type='list'),
        availability_zones=dict(default=[], type='list'),
        availability_zone_ids=dict(default=[], type='list'),
        instance_types=dict(default=[], type='list'),
    )
    extra_spec = dict(
        all_zone_ids=dict(type='bool'),
        only_common_types=dict(type='bool'),
    )
    argument_spec.update(main_spec)
    argument_spec.update(extra_spec)

    module = AnsibleModule(argument_spec=argument_spec,
                           mutually_exclusive=main_spec.keys())

    if not HAS_BOTO3:
        module.fail_json(msg='boto3 required for this module')

    region, ec2_url, aws_connect_params = get_aws_connection_info(module, boto3=True)

    if region:
        connection = boto3_conn(module, conn_type='client', resource='ec2',
                                region=region, endpoint=ec2_url,
                                **aws_connect_params)
    else:
        module.fail_json(msg="region must be specified")
        # not really, but PyC doesn't know
        return

    regions_param = module.params.get('regions')
    az_list_param = module.params.get('availability_zones')
    azid_list_param = module.params.get('availability_zone_ids')
    if module.boolean(module.params.get('all_zone_ids')):
        if azid_list_param:
            module.fail_json(msg='all_zone_ids= cannot be used with availability_zone_ids=')
            return
        azid_list_param = [az['zone_id'] for az in get_az_id_list(connection)]
    if regions_param:
        location_type = 'region'
        location_filter = {'Name': 'location', 'Values': regions_param}
    elif az_list_param:
        location_type = 'availability-zone'
        filter_values = az_list_param
        location_filter = {'Name': 'location', 'Values': filter_values}
    elif azid_list_param:
        location_type = 'availability-zone-id'
        filter_values = azid_list_param
        location_filter = {'Name': 'location', 'Values': filter_values}
    else:
        location_type = 'region'
        location_filter = {'Name': 'location', 'Values': [region]}

    i_types = get_instance_types_by_filter(connection,
                                           location_type=location_type,
                                           filters=[location_filter])

    ansible_facts = {}

    if module.boolean(module.params.get('only_common_types')):
        common_types = None
        for i_type_set in i_types.values():
            if common_types is None:
                common_types = i_type_set
                continue
            common_types = common_types.intersection(i_type_set)
        common_types = sorted(list(common_types))
        ansible_facts['ec2_common_instance_types'] = common_types
        # it doesn't even make sense to return the whole dict
        # but we leave it for output consistency
        # i_types = {k: common_types for k in i_types.keys()}

    # just a visual nicety
    i_types = {k: sorted(list(v)) for k, v in i_types.items()}

    changed = bool(i_types)
    ansible_facts['ec2_instance_types_by_location'] = i_types
    module.exit_json(changed=changed,
                     ansible_facts=ansible_facts)


if __name__ == '__main__':
    main()
