#! /usr/bin/env python
# coding=utf-8
import json
import os
import re
import sys

from troposphere import (
    Cidr,
    GetAZs,
    Join,
    Output,
    Parameter,
    Ref,
    Region,
    Select,
    Template,
)

from troposphere.ec2 import (
    Instance,
    InternetGateway,
    Route,
    RouteTable,
    SecurityGroup,
    SecurityGroupEgress,
    SecurityGroupIngress,
    Subnet,
    SubnetRouteTableAssociation,
    Tag,
    VPC,
    VPCGatewayAttachment,
)

from troposphere.iam import (
    InstanceProfile,
    Policy,
    Role,
)

default_tags = [
    Tag(key='project', value='openraven'),
]


def create_vpc(t: Template):
    allow_ports = [22, 80]

    vpc_cidr = t.add_parameter(Parameter(
        'VpcCidr',
        AllowedPattern='/'.join([r'\.'.join([r'\d{1,3}'] * 4), r'\d{1,2}']),
        Type='String',
        Default='10.128.0.0/16',
        Description='the IP CIDR to assign to the VPC',
    ))

    vpc = t.add_resource(VPC(
        'OpenRavenVpc',
        CidrBlock=Ref(vpc_cidr),
        # shrug
        EnableDnsSupport=True,
        Tags=default_tags,
    ))

    igw = t.add_resource(InternetGateway(
        'IGW',
        Tags=default_tags,
    ))

    igw_att = t.add_resource(VPCGatewayAttachment(
        'IGWAttachment',
        InternetGatewayId=Ref(igw),
        VpcId=Ref(vpc),
    ))

    route_table = t.add_resource(RouteTable(
        'DefaultRouteTable',
        Tags=default_tags,
        VpcId=Ref(vpc),
    ))

    # it appears the $VpcCidr -> local is created automatically
    t.add_resource(Route(
        'DefaultRoute',
        DependsOn=igw_att,
        DestinationCidrBlock='0.0.0.0/0',
        GatewayId=Ref(igw),
        RouteTableId=Ref(route_table),
    ))

    subnet_1a = t.add_resource(Subnet(
        'PublicSubnet1A',
        AvailabilityZone=Select(0, GetAZs(Region)),
        # /20 is 12 host bits
        # and squares up with terraform.tfvars
        CidrBlock=Select(0, Cidr(Ref(vpc_cidr), 1, 12)),
        MapPublicIpOnLaunch=True,
        Tags=default_tags,
        VpcId=Ref(vpc),
    ))

    t.add_resource(SubnetRouteTableAssociation(
        'PublicSubnet1ARt',
        RouteTableId=Ref(route_table),
        SubnetId=Ref(subnet_1a),
    ))

    vpc_sg = t.add_resource(SecurityGroup(
        'Sg',
        GroupName='openraven provisioning',
        GroupDescription='allow access to the OpenRaven provisioning container',
        Tags=default_tags,
        VpcId=Ref(vpc),
    ))

    for p in allow_ports:
        t.add_resource(SecurityGroupIngress(
            'SgIngress{}'.format(p),
            GroupId=Ref(vpc_sg),
            CidrIp='0.0.0.0/0',
            FromPort=p,
            ToPort=p,
            IpProtocol='tcp',
        ))
        del p

    t.add_resource(SecurityGroupEgress(
        'SgEgress',
        GroupId=Ref(vpc_sg),
        CidrIp='0.0.0.0/0',
        FromPort=-1,
        ToPort=-1,
        IpProtocol='-1',
    ))


def main(argv):
    output_fn = '-'
    if len(argv) > 1:
        output_fn = argv[1]
        print('Saving output to "{}"'.format(output_fn), file=sys.stderr)

    instance_type = 't3.small'
    # ami-0d6e65dd062be7be2 := open/kubespray:11
    the_image_id = os.getenv('AMI', 'ami-0d6e65dd062be7be2')
    packer_manifest_fn = 'manifest.json'

    if os.path.isfile(packer_manifest_fn):
        with open(packer_manifest_fn) as fh:
            packer_info = json.load(fh)
        builds = packer_info.get('builds', [])
        if not builds:
            print('Expected "{}" to contain a non-empty "builds":[] structure',
                  file=sys.stderr)
            return 1
        builds = sorted(builds, key=lambda x: x['build_time'], reverse=True)
        build = builds[0]
        art_id = build['artifact_id']
        the_image_id = re.sub(r'^us-[^:]+:(ami-.+)', r'\1', art_id)
        print('Using "{}" from the packer output "{}"'
              .format(the_image_id, packer_manifest_fn), file=sys.stderr)

    t = Template(Description='Open Raven stack')
    # this is the version from "new template" in the designer web UI
    # and is defined in the manual as "AWSTemplateFormatVersion"
    t.version = '2010-09-09'

    key_name = t.add_parameter(
        Parameter(
            'KeyName',
            ConstraintDescription='must be the name of an existing EC2 KeyPair.',
            Description='Name of an existing EC2 KeyPair to enable SSH access to the instance',
            Type='AWS::EC2::KeyPair::KeyName',
        ))

    role = t.add_resource(Role(
        'EggRole',
        AssumeRolePolicyDocument={
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": "sts:AssumeRole",
                    "Principal": {
                        "Service": [
                            "ec2.amazonaws.com",
                        ]
                    }
                }
            ]
        },
        Description='actions taken by the Open Raven bootstrap EC2 instance',
        ManagedPolicyArns=[
            'arn:aws:iam::aws:policy/AmazonEC2FullAccess',
            # BOOOOOOOO
            'arn:aws:iam::aws:policy/IAMFullAccess',
        ],
        Policies=[Policy(
            PolicyName=Join('', [Ref('AWS::StackName'), '-bootstrap']),
            PolicyDocument={
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Action": [
                            "iam:AttachRolePolicy",
                            "iam:AttachRoleToInstanceProfile",
                            "iam:CreateInstanceProfile",
                            "iam:CreatePolicy",
                            "iam:CreateRole",
                            "iam:CreateServiceLinkedRole",
                            "iam:GetPolicy*",
                            "iam:GetRole*",
                            "iam:ListPolic*",
                            "iam:ListRoles",
                            "iam:PassRole"

                        ],
                        "Resource": "*"
                    }
                ]
            }
        )],
        RoleName=Join('', [Ref('AWS::StackName'), '-bootstrap']),
        Tags=default_tags,
    ))

    inst_profile = t.add_resource(InstanceProfile(
        'EggInstProfile',
        Roles=[Ref(role)],
        InstanceProfileName=Join('', [Ref('AWS::StackName'), '-bootstrap']),
    ))
    create_vpc(t)

    inst = t.add_resource(Instance(
        'RavenEgg',
        IamInstanceProfile=Ref(inst_profile),
        ImageId=the_image_id,
        InstanceType=instance_type,
        KeyName=Ref(key_name),
        SecurityGroupIds=[Ref(t.resources['Sg'])],
        SubnetId=Ref(t.resources['PublicSubnet1A']),
        Tags=default_tags,
    ))
    t.add_output(Output(
        'PublicIp',
        Description='the public IP address of the bootstrap instance',
        Value=inst.get_att('PublicIp'),
    ))

    fh = sys.stdout if '-' == output_fn else open(output_fn, 'w')
    body = t.to_json() if output_fn.endswith('.json') else t.to_yaml()
    # cfn-lint whines about `: "true"`
    body = re.sub(r''': ['"](true|false)["']''', r': \1', body)
    fh.write(body)
    fh.close()


if __name__ == '__main__':
    sys.exit(main(sys.argv))
