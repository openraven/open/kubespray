# Available Approaches
* AMI
  * _bare_
  * _CloudFormation_
* CloudFormation
* Container
  * ECS
    * _fake-o_ by just giving ECR marketplace docker image and &#x1F340;
    * _CloudFormation_ this just delegates to CF to host the ECS Task
  * EKS _untested, since that's not where we want to go_

## AMI

This appears to have some of the simplest getting-started knobs to it, with
the disadvantage that one must already have VPC, Subnet, some kind of SG(?),
and either a KeyPair or the willingness to go through the KP creation flow

## CloudFormation

This has the best "up front" UX, and the worst(?) _for real_ UX

The up-front part shows pretty pictures of the proposed CF stack, includes
the services that will be used (including listing optional ones), and in many
cases can provide cost estimates

But, that's just the pre-launch experience: once you subscribe, you get the full
CF launch page, filled with arguably a bazillion configuration knobs 

## Container - ECS

It turns out, this is subdivided into two separate approaches, one of which is
just 100% damn cheating, and the other which is about 25% cheating:

* "subscribing" just generates a marketplace ECR docker image URL
  and the instructions consist of
  "here, bub, run `docker run $ECR_IMAGE` and &#x1F340; to ya"
  
  To add insult to injury, one must provide **actual** ECR creds, even in the
  case of a "free" image just because you subscribed to it

* The other path provides a CloudFormation template that boots up a ECR cluster
  with the ECS Task in it

## Container - EKS

Although this does pose an interesting deployment model:
Bring Your Own Kubernetes Cluster&trade; which we might not use for **running**
but we could very, very easily use for _bootstrapping_
 
