#!/usr/bin/env bash
set -euo pipefail
[[ -n "${ami_extra:-}" ]] && set -x
echo '== provision environment' >&2
set >&2
echo '==/provision environment' >&2

docker_image="docker.openraven.io/open/kubespray:${release_id}"
listen_port="${listen_port:-80}"
httpd_port="8000"
systemd_service="openraven.service"

# sadly, there does not appear to be any stable URL and thus no shasums for it
# but thankfully it appears to be "pip install"-able
cfn_init_url='https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz'
cfn_init_venv=/opt/aws-cfn-bootstrap

pypy_tar_url='https://bitbucket.org/squeaky/portable-pypy/downloads/pypy3.5-7.0.0-linux_x86_64-portable.tar.bz2'
# this was copied from the Ignition script, which requires sha512
pypy_tar_sha512=e4df0d90cd004cc3c5ffe615f8c730e9a6c9cb7935ed1dd0f1313fc845926d75314c3b107a414fe3a75e847464ae783c1d368a5eb6e51c3fbfa44499167dea69

# fucking cfn-bootstrap is python 2.7 and there doesn't appear to be any
# reasonable python3 ports of it (the Mesosphere one is half-baked at best,
# and based off an unknown upstream version)
pypy27_tar_url='https://bitbucket.org/squeaky/portable-pypy/downloads/pypy-7.1.1-linux_x86_64-portable.tar.bz2'
pypy27_tar_sha512=dadab2cac2f26b2e018b8cf2c284d59cda8530e9481b6bbcb0ac556a689fffce78c7475b805f78a77429f886a2c9b06e8c6d60b161c158a89d6b58d507eede9f

generate_install_systemd_service() {
    cat >/etc/systemd/system/${systemd_service}<<FOO
[Unit]
Description = Launches the Open Raven provisioning container
[Install]
WantedBy = multi-user.target
[Service]
Type = oneshot
# RemainAfterExit = true
ExecStart = /usr/bin/docker run --name or-ce \
--env AUTO_LAUNCH=true \
--publish ${listen_port}:${httpd_port} \
--volume /home/core:/home/core \
--volume /root:/root \
${docker_image}
FOO
}

register_install_httpd_service() {
    systemctl daemon-reload
    if ! systemctl enable ${systemd_service}; then
        journalctl --lines=250 -xe
        return 1
    fi
}

pre_pull_the_docker_image() {
    docker pull ${docker_image}
}

pre_pull_pypy() {
    local url="$1"
    local sha5="$2"
    output_fn=/root/$(basename "$url")
    curl -fsSLo "$output_fn" "${url}"
    echo "$sha5  $output_fn" | sha512sum -c -
    mkdir -p /opt/bin
    tar -xjf "$output_fn" -C /opt/bin
    rm "$output_fn"
}

install_cfn_bootstrap() {
    # the shebang for virtualenv-pypy is fubared
    /opt/bin/pypy2/bin/pypy -u /opt/bin/pypy2/bin/virtualenv-pypy ${cfn_init_venv}
    ${cfn_init_venv}/bin/pip install ${cfn_init_url}
    if ${cfn_init_venv}/bin/cfn-init --help | grep -iq stack; then
      return 0
    fi
    echo 'It appears cfn-init is unwell' >&2
    return 1
}

generate_install_systemd_service
register_install_httpd_service
pre_pull_the_docker_image

pre_pull_pypy "$pypy_tar_url"   "$pypy_tar_sha512"
pre_pull_pypy "$pypy27_tar_url" "$pypy27_tar_sha512"

mv -iv /opt/bin/pypy3.*linux* /opt/bin/pypy3
ln -sf pypy3/bin/pypy3 /opt/bin/python

mv -iv /opt/bin/pypy-7* /opt/bin/pypy2
# this is a concession to kubespray
touch /opt/bin/.bootstrapped

install_cfn_bootstrap
