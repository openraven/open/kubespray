FROM docker.openraven.io/open/build-image:790
ARG RELEASE_ID
ENV RELEASE_ID=$RELEASE_ID
EXPOSE 8000
COPY .git /kubespray/.git
WORKDIR   /kubespray
COPY public ./public
RUN git reset --hard HEAD
ENTRYPOINT ["./docker-entrypoint.sh"]
