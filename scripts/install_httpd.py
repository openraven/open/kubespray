#! /usr/bin/env python
# coding=utf-8
import json
import os.path
import re
import sys
import time
import traceback
import typing

from ansible_runner import Runner
from ansible_runner.runner_config import RunnerConfig
from http.server import (SimpleHTTPRequestHandler, ThreadingHTTPServer)
import requests
from urllib.parse import parse_qs

"""
When running in docker-machine, the "uname" will be
> Operating System: Boot2Docker 19.03.5 (TCL 10.1)
``-v /proc:/hostproc ... grep vboxsf /hostproc/modules``
but there does not appear to be an obvious way to get the VBox network list
since that mapping is happening on the **host**
"""

BOOTSTRAP_JSON_FILENAME = '/kubespray/bootstrap.json'
"""
Contains the extra-vars filename used for ``bootstrap.yml``

An example::

    {
      "aws_region": "us-west-1",
      "aws_ssh_keyname": "devops",
      "cluster_name": "cluster0",
      "cluster_size": "small",
      // **optionally**, if $HOME/.aws is not mounted
      "aws_access_key_id": ""
      "aws_secret_access_key": ""
      // TODO
      // "aws_mfa_device" and "aws_session_token" and "aws_security_token"
    }

"""

CLUSTER_JSON_FILENAME = '/kubespray/cluster.json'
"""
Contains the extra-vars filename used for ``cluster.yml``

Be forewarned these keys differ from :ref:`BOOTSTRAP_JSON_FILENAME`
because some of the names are "reserved" in kubespray. Maybe we should
rename the bootstrap to match them, but here's where we are.

An example::

    {
      "aws_kubernetes_cluster_id": "my-cluster",
      "aws_default_region": "us-west-1",
      "aws_ssh_keyname": "devops",
      "cluster_size": "large",
      "helm_enabled": true,
      "kubeconfig_localhost": true
    }
"""

ANSIBLE_RUNNER_ARTIFACT_DIR = '/kubespray/artifacts'
"""The directory into which ``ansible-runner`` writes its state files"""

BOOTSTRAP_GROUP_ID_KEY = 'okta_group_id'
"""The key in bootstrap.json wherein the cluster's Group Id should go"""

OKTA_GROUP_ID_ENV_VAR = 'GROUP_ID'
"""The environment variable containing the Okta GroupId used to identify this cluster"""

PUBLIC_HOSTNAME_CLUSTER_JSON_KEY = 'openraven_ingress_hostname'
"""the key in the JSON extra-vars containing the public hostname of the Open Raven cluster"""

SUPPORT_EMAIL = 'support@openraven.com'

CALLBACK_URL = 'https://api.openraven.com/api/account/activate/{outcome}/{groupId}'


def get_cluster_group_id():
    return os.getenv(OKTA_GROUP_ID_ENV_VAR)


def get_cluster_public_url(extra_vars):
    """
    :type extra_vars: typing.Dict[str, typing.Any]
    :rtype: str
    """
    public_hostname = extra_vars.get(PUBLIC_HOSTNAME_CLUSTER_JSON_KEY)
    # yes, "http:" for the time being, until we straighten out cert provisioning
    public_url = 'http://{}'.format(public_hostname)
    return public_url


def send_callback(cluster_url, successful):
    """
    :param str cluster_url: the entry-point for the cluster, which should be authorized in Okta
    :param bool successful: was the install successful?
    """
    g_id = get_cluster_group_id()
    if not g_id:
        print('Skipping callback process due to missing ${}'
              .format(OKTA_GROUP_ID_ENV_VAR), file=sys.stderr)
        return
    outcome = 'success' if successful else 'failure'
    u = CALLBACK_URL.format(groupId=g_id, outcome=outcome)
    post_body = json.dumps({'cluster-url': cluster_url}).encode('utf-8')
    c_len = len(post_body)
    c_type = 'application/json;charset=utf-8'
    headers = {
        'content-length': str(c_len),
        'content-type': c_type,
    }
    r = requests.post(u, data=post_body, headers=headers)
    r_status = r.status_code
    if r_status >= 300:
        print('EGAD, unexpected callback status code: {}\nbody: <<{}>>'
              .format(r_status, repr(r.text)), file=sys.stderr)


# noinspection PyPep8Naming
class ProvisioningHandler(SimpleHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'

    def do_GET(self):
        path = self.path  # type: str
        self.log_message('welcome, GET path="%s"', path)
        if path.startswith('/event-source'):
            try:
                if not self.handle_event_source_GET():
                    # this will trip the send_callback below
                    raise IOError('Bogus provision')

                with open(CLUSTER_JSON_FILENAME) as fh:
                    cluster_json = json.load(fh)

                cluster_url = get_cluster_public_url(cluster_json)
                send_callback(cluster_url, True)
                print(('\n\nInstall complete!\n'
                       'you can press control-c to shut down this server\n'
                       'You can access the application at:\n'
                       '  {}')
                      .format(cluster_url))
            except Exception:
                send_callback('', False)
                raise
            return
        elif path == '/aws-role':
            return self.handle_aws_role_GET()
        elif path == '/okta-group-id':
            return self.handle_okta_group_id_GET()
        elif path == '/public-url':
            return self.handle_public_url_GET()
        elif path == '/access-aws':
            return self.handle_access_aws_GET()

        # otherwise deliver any files found in "public/"
        super(ProvisioningHandler, self).do_GET()

    def handle_access_aws_GET(self):
        """this is a concession to React routing and is handy for development"""
        self.send_response(302)
        self.send_header('location', '/')
        self.send_header('content-length', '0')
        self.end_headers()
        self.wfile.flush()

    def handle_public_url_GET(self):
        if not os.path.exists(CLUSTER_JSON_FILENAME):
            self.log_error('Unable to open "%" because it does not exist',
                           CLUSTER_JSON_FILENAME)
            self.send_response(404)
            self.send_header('content-length', '0')
            self.end_headers()
            self.wfile.flush()
            return
        with open(CLUSTER_JSON_FILENAME) as fh:
            cluster_json = json.load(fh)

        public_url = get_cluster_public_url(cluster_json)
        result = {
            "public-url": public_url,
        }
        return self._send_json(result)

    def handle_okta_group_id_GET(self):
        g_id = get_cluster_group_id()
        if not g_id:
            return self._send_problem_json({
                "type": "https://openraven.com/errors/missing-group-id",
                "title": "Missing $GROUP_ID in the container environment",
            }, status=404)
        return self._send_json({"oktaGroupId": g_id})

    def handle_aws_role_GET(self):
        iam_dir = '/kubespray/contrib/aws_iam'
        policy_docs = []
        result = {'Policies': policy_docs}
        iam_files = [
            'kubernetes-master-policy.json',
            'kubernetes-minion-policy.json',
        ]
        for fn in iam_files:
            fqfn = os.path.join(iam_dir, fn)
            if not os.path.exists(fqfn):
                continue
            with open(fqfn) as fh:
                policy_doc = json.load(fh)
            policy_docs.append(policy_doc)
        return self._send_json(result)

    def handle_event_source_GET(self):
        # whether to send back **every** playbook event
        # which is quite chatty; they are still persisted on disk for later,
        # regardless of this setting
        send_verbose_events = 'verbose=true' in self.path
        # does the browser prefer the "generic", unnamed events?
        send_named_events = 'onmessage=true' not in self.path
        ansible_verbosity = 2

        def flat_json_dumps(obj):
            result = json.dumps(obj, indent=None, separators=(',', ':'))
            result = result.lstrip('\n').rstrip('\n')
            return result

        def write_flat_json_event_data(event_name, obj):
            """
            This method is **terminal**,
            as it flushes the ``EventSource`` final newline

            :param str event_name: the logical EventSource name
            :param typing.Dict obj: the thing to serialize and send
            """
            try:
                if send_named_events:
                    self.wfile.write(b'event: ')
                    self.wfile.write(event_name.encode('utf-8'))
                    self.wfile.write(b'\n')
                else:
                    # clone for mutation
                    obj = dict(obj)
                    obj['$eventName'] = event_name

                self.wfile.write(b'data: ')
                self.wfile.write(flat_json_dumps(obj).encode('utf-8'))
                self.wfile.write(b'\n')

                self.wfile.write(b'\n')
                self.wfile.flush()
            except BrokenPipeError as bpe:
                self.log_error('EGAD, the Browser disconnected!'
                               ' Continuing without writing EventSource: %r', bpe)
                self.wfile = open('/dev/null', 'wb')

        def event_handler(evt):
            """

            "event": "playbook_on_play_start"
            "event": "playbook_on_start"
            "event": "playbook_on_stats"
            "event": "playbook_on_task_start"
            "event": "runner_on_failed"
            "event": "runner_on_ok"
            "event": "runner_on_skipped"
            "event": "verbose"

            :param typing.Dict evt: an ansible event
            :return: whether to write out this event to the .json file on disk
            """
            event_kind = evt.get('event', '')
            # print('\t\t\t\tansibleEvent[{}]'.format(event_kind), file=sys.stderr)
            if send_verbose_events or event_kind != 'verbose':
                write_flat_json_event_data('ansibleEvent', evt)
            return True

        def finished_callback(runner):
            """:param Runner runner: the runner that just finished"""
            write_flat_json_event_data('ansibleExitcode', {
                "exitCode": runner.rc,
                "module": runner.config.module,
                "playbook": runner.config.playbook,
            })

        def status_handler(status_dict, **kwargs):
            # :param Dict status_dict: with keys ``status`` and ``runner_ident``
            # :keyword RunnerConfig runner_config:
            if 'runner_config' not in kwargs:
                self.log_message('Expected runner_config in kwargs')
            run_config = kwargs['runner_config']
            write_flat_json_event_data('ansibleStatus', {
                "status": status_dict.get('status', 'unknown'),
                "module": run_config.module,
                "playbook": run_config.playbook,
            })

        def invoke_runner(run_config, with_callbacks=True):
            """
            :type run_config: RunnerConfig
            :param bool with_callbacks: if True, wire in the ``EventSource`` callbacks
            """
            run_config.verbosity = ansible_verbosity
            run_config.prepare()
            self.log_message('command = %r', run_config.generate_ansible_command())
            # ``cancel_callback`` is a callable that returns True/False for
            # whether the playbook should continue running
            r = Runner(config=run_config, remove_partials=False)
            if with_callbacks:
                r.event_handler = event_handler
                r.finished_callback = finished_callback
                r.status_handler = status_handler
            try:
                run_result = r.run()
            except Exception as ex:
                self.log_error('Kaboom: %s', ex)
                if with_callbacks:
                    write_flat_json_event_data('ansibleError', {
                        "traceback": traceback.format_exc()
                    })
                # then, fall-through to send the "closing" event
                return False
            if run_result is None or not isinstance(run_result, (list, tuple)):
                self.log_error('run() gave an unexpected response: %r',
                               run_result)
                return False

            if run_config.module:
                run_result_label = run_config.module
            elif run_config.playbook:
                run_result_label = run_config.playbook
            else:
                run_result_label = str(run_config)
            self.log_message('run("%s") := %r', run_result_label, run_result)
            final_status, run_rc = run_result
            # TODO safer to test the RC or the status is "successful"?
            return run_rc == 0

        # noinspection PyUnusedLocal
        def run_module(extra_vars, inventory, module, module_args, with_callbacks=True):
            envvars = {
                'ANSIBLE_CONFIG': '/kubespray/ansible.cfg',
            }
            c = RunnerConfig(private_data_dir='/kubespray',
                             envvars=envvars,
                             extravars=extra_vars,
                             host_pattern='all',
                             inventory=inventory,
                             module=module, module_args=module_args)
            return invoke_runner(c, with_callbacks)

        def run_playbook(extra_vars, inventory, playbook_filename):
            """
            :param str extra_vars: the filename that will be provided as extra vars to the playbook
            :param str|list[str] inventory: the filename, which can be a script,
            or a list of the actual inventory hosts
            :param str playbook_filename: as it says
            :return: True if everything was a-ok
            :rtype: bool
            """
            envvars = {
                'ANSIBLE_CONFIG': '/kubespray/ansible.cfg',
            }
            if playbook_filename == 'cluster.yml':
                envvars['ANSIBLE_BECOME'] = 'yes'
            c = RunnerConfig(private_data_dir='/kubespray',
                             envvars=envvars,
                             extravars=extra_vars,
                             inventory=inventory,
                             playbook=playbook_filename)
            return invoke_runner(c)

        if os.path.exists(ANSIBLE_RUNNER_ARTIFACT_DIR):
            run_uuids = os.listdir(ANSIBLE_RUNNER_ARTIFACT_DIR)
            if run_uuids:
                self.log_message('Skipping duplicate event-source request,'
                                 ' as %s is already running or has run',
                                 ','.join(run_uuids))
                self.send_response_only(200)
                self._add_cors_headers()
                self.send_header('cache-control', 'no-cache')
                self.send_header('content-type', 'text/event-stream')
                self.end_headers()
                write_flat_json_event_data('ansibleStop', {})
                # pretend everything is fine
                return True

        # > Last-Event-ID: 19
        self.send_response_only(200)
        self._add_cors_headers()
        self.send_header('cache-control', 'no-cache')
        self.send_header('content-type', 'text/event-stream')
        self.end_headers()

        write_flat_json_event_data('ansibleStart', {})

        cluster_ok = False
        if run_playbook(BOOTSTRAP_JSON_FILENAME,
                        ['localhost,'], 'bootstrap.yml'):
            inventory_script = '/kubespray/contrib/aws_inventory/tag_based_inventory.py'
            cluster_ok = run_playbook(CLUSTER_JSON_FILENAME, inventory_script, 'cluster.yml')

        write_flat_json_event_data('ansibleStop', {})
        self.log_message('cluster provision success = %s', cluster_ok)
        return cluster_ok

    def do_POST(self):
        self.log_message('POST path="%s"', self.path)
        c_len = self.headers.get('content-length')
        if c_len is not None:
            c_len = int(c_len)
        body = self.rfile.read(c_len)
        c_type = self.headers.get('content-type', '')

        if c_type.startswith('application/x-www-form-urlencoded'):
            body = body.decode('utf-8')
            post_dict = parse_qs(body)  # type: typing.Dict[str, typing.List[str]]
            payload = {k: v[0] for k, v in post_dict.items() if k != 'submit'}
            del post_dict
        elif c_type.startswith('application/json'):
            try:
                payload = json.loads(body)
            except Exception as e:
                self.log_error('BOGUS BODY <<%s>>: %s', repr(body), e)
                self._send_problem_json({
                    "type": "https://openraven.com/errors/malformed-json",
                    "title": "Input body was not JSON",
                    "detail": "bootstrap POST body must be well-formed JSON",
                })
                return
        else:
            self.log_error('Unknown content-type "%s"; body=<<%s>>',
                           c_type, repr(body))
            self._send_problem_json({
                "type": "https://openraven.com/errors/unknown-content-type",
                "title": "Unknown bootstrap content-type",
                "detail": "bootstrap POST body must contain one of"
                          " form-urlencoded or application/json data",
            })
            return

        self.log_message('Received %r', payload)
        if not payload:
            self._send_problem_json({
                "type": "https://openraven.com/errors/empty-bootstrap-json",
                "title": "the bootstrap payload is empty",
                "detail": "bootstrap POST body must contain some values",
            })
            return

        g_id = get_cluster_group_id()
        if g_id:
            payload.update({
                BOOTSTRAP_GROUP_ID_KEY: g_id,
            })
        else:
            self.log_error('Unable to find Cluster Group ID in the environment ${}'
                           .format(OKTA_GROUP_ID_ENV_VAR))

        if os.path.exists(BOOTSTRAP_JSON_FILENAME):
            self.log_message('Merging payload into existing "%s"', BOOTSTRAP_JSON_FILENAME)
            with open(BOOTSTRAP_JSON_FILENAME) as fh:
                bsev = json.load(fh)  # type: typing.Dict
                bsev.update(payload)
                payload = bsev

        with open(BOOTSTRAP_JSON_FILENAME, 'w') as fh:
            json.dump(payload, fh)

        self.send_response_only(200)
        self._add_cors_headers()
        response_body = json.dumps({
            "location": "/event-source",
        }, indent=None).encode('utf-8')
        self.send_header('content-length', len(response_body))
        self.send_header('content-type', 'application/json;charset=utf-8')
        self.end_headers()
        self.wfile.write(response_body)
        self.wfile.flush()

    def do_OPTIONS(self):
        self.send_response_only(204)
        self._add_cors_headers()
        self.send_header('content-type', 'text/plain;charset=utf-8')
        self.send_header('content-length', '0')
        self.end_headers()

    def _add_cors_headers(self):
        their_origin = self.headers.get('origin', '*')
        allowed_methods = [
            'GET',
            'OPTIONS',
            'POST',
        ]
        expose_headers = [
            'Cache-Control',
            'Content-Language',
            'Content-Type',
            'Expires',
            'Last-Modified',
            'Location',
            'Pragma',
        ]
        cors_max_age = '86400'
        self.send_header('Access-Control-Allow-Origin', their_origin)
        self.send_header('Access-Control-Allow-Methods',
                         ','.join(allowed_methods))
        self.send_header('Access-Control-Allow-Headers',
                         ','.join(expose_headers))
        self.send_header('Access-Control-Expose-Headers',
                         ','.join(expose_headers))
        self.send_header('Access-Control-Max-Age', cors_max_age)

    def _send_json(self, reply):
        body = json.dumps(reply, indent=0).encode('utf-8')
        self.send_response(200)
        self._add_cors_headers()
        self.send_header('content-length', len(body))
        self.send_header('content-type', 'application/json;charset=utf-8')
        self.end_headers()
        self.wfile.write(body)
        self.wfile.flush()

    def _send_problem_json(self, problem_json_payload, status=500):
        """
        :type problem_json_payload: typing.Dict[str,str]
        :see: https://tools.ietf.org/html/rfc7807
        """
        err_msg = json.dumps(problem_json_payload).encode('utf-8')
        self.send_response_only(status)
        self._add_cors_headers()
        self.send_header('content-length', len(err_msg))
        self.send_header('content-type', 'application/problem+json;charset=utf-8')
        self.end_headers()
        self.wfile.write(err_msg)
        self.wfile.flush()


def main(argv):
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', action='store', default='8000')
    args = parser.parse_args(argv[1:])
    # change into the provisioning-ui directory
    # so self.send_head() will resolve correctly
    os.chdir('/kubespray/public')
    my_hostname = '127.0.0.1'
    d_host = os.getenv('DOCKER_HOST')
    if d_host:
        ma = re.search(r'^[^:]+://([^:/]+).*', d_host)
        if ma is not None:
            my_hostname = ma.group(1)
            del ma
        else:
            print(('Unable to discover true docker host from "{}",'
                   ' so the URL below might not work').format(d_host),
                  file=sys.stderr)
    port = int(args.port)
    listen_addr = ('0.0.0.0', port)
    print('Please go to http://{}:{}'.format(my_hostname, port))
    server = ThreadingHTTPServer(listen_addr, ProvisioningHandler)
    server.daemon_threads = False
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    print('Shutting down ...\n'
          'if this takes more than a few seconds, press control-c again',
          file=sys.stderr)
    important_files = [
        '../contrib/terraform/aws/terraform.tfstate',
        '../contrib/aws_inventory/artifacts/admin.conf',
        # they _shouldn't_ need this but could-a should-a etc etc
        '/root/.ssh/ansible.pem',
    ]
    for fn in important_files:
        fqfn = os.path.abspath(fn)
        if os.path.exists(fqfn):
            print(('Be sure you save "{fn}" before removing the container;\n'
                   '  you can save it via `docker cp or-ce:{fn} .`')
                  .format(fn=fqfn), file=sys.stderr)
    print('If we can be of any help, please reach out to "{}"'
          .format(SUPPORT_EMAIL), file=sys.stderr)
    server.shutdown()


if __name__ == '__main__':
    sys.exit(main(sys.argv))
