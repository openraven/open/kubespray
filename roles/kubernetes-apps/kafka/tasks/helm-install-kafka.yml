- name: Helm Repo Install
  debug:
    msg: "Helm Repo support"
    verbosity: 4
- name: Helm Repo Vars
  debug:
    var: "{{ item }}"
    verbosity: 4
  with_items:
    - helm_repos
    - helm_charts

# This is duplicately declared in case a role is applied individually.
- name: declare generated kubeconfig
  set_fact:
    inventory_kubeconfig: --kubeconfig {{ inventory_dir }}/artifacts/admin.conf

- name: List all charts installed to see if our chart is already installed
  shell: helm ls -q {{ inventory_kubeconfig }}
  register: helm_charts_installed
  delegate_to: localhost

# We're avoiding the built-in helm support for ansible as it is unsupported in the ansible community. Additionally,
# built-in helm support relies on pyhelm and grpcio, this adds complexity to our deployment environment that we do not
# want to manage. Favoring a simpler shell based approach with an assumption of helm being either installed or
# dockerized into our environment. Error found: failed to connect to all addresses.
- name: List all repos to see if our repo is already installed
  shell: helm repo list {{ inventory_kubeconfig }} | awk '/^NAME/{next;}{ print $1}'
  register: helm_repos_installed
  delegate_to: localhost

- name: Install helm repo
  shell: helm repo add {{ item.name }} {{ item.repo }} {{ inventory_kubeconfig }}
  loop: "{{ helm_repos }}"
  when: item.name not in helm_repos_installed.stdout_lines
  delegate_to: localhost

- name: create temporary build directory
  tempfile:
    state: directory
    suffix: temp
  register: tmp_dir
  delegate_to: localhost

- name: render the chart values file
  template:
    src: helm/{{ item.chart_name }}-values.{{ item.version }}.yml
    dest: "{{ tmp_dir.path }}/{{ item.chart_name }}-values.{{ item.version }}.yml"
  loop: "{{ helm_charts }}"
  delegate_to: localhost

# Timeout of 7.5 minutes because it can take over 5 minutes for all 3 instances to come up.
- name: Install/Upgrade helm charts
  when: item.name not in helm_charts_installed.stdout_lines
  shell: >
    helm
    {{ helm_verb }}
    {{ item.name }}
    {{ item.chart }}
    --namespace {{ item.namespace }}
    --version {{ item.version }}
    --values {{ tmp_dir.path }}/{{ item.chart_name }}-values.{{ item.version }}.yml
    {% if (cluster_size|d("large")) == "small" %}
    --set replicaCount=1
    --set zookeeper.replicaCount=1
    {% endif %}
    --timeout 450
    {{ inventory_kubeconfig }}
    --wait
  loop: "{{ helm_charts }}"
  vars:
    helm_verb: "{{ 'install --name '
    if item.chart_name not in helm_charts_installed.stdout_lines
    else 'upgrade ' }}"
  delegate_to: localhost

- name: Cleanup temp file
  file:
    path: "{{ tmp_dir.path }}"
    state: absent
  when: tmp_dir.path is defined
  delegate_to: localhost

#ansible test version range for comparing semver based version identifiers and applying things like upgrades conditionally
#TODO: Improve functionality to compare versions for valid upgrades ({% if kubeadm_version is version('v1.14.0', '>=') %})
