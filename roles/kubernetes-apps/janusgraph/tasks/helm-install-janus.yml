- name: declare inventory_dir, if necessary
  when: inventory_dir is not defined
  set_fact:
    inventory_dir: '{{ ansible_inventory_sources | first | dirname }}'

- name: declare kubeconfig, if necessary
  when: kubeconfig is not defined
  set_fact:
    kubeconfig: '{{ inventory_dir }}/artifacts/admin.conf'

- name: pull down the Cassandra password, if necessary
  when: janus_cassandra_password is not defined
  block:
  - name: gather the cassandra password Secret
    command: >
      kubectl -n {{ janusgraph_cassandra_ns }}
      get -o json
      secret {{ janusgraph_cassandra_secret_name }}
    register: cassandra_secret_json
    no_log: ansible_verbosity < 2
    environment: &kubeconfig
      KUBECONFIG: '{{ kubeconfig }}'

  - name: decode cassandra password
    no_log: true
    set_fact:
      janus_cassandra_password: >-
        {{-
        ( cassandra_secret_json.stdout
        | from_json
        ).data[janusgraph_cassandra_secret_key]
        | b64decode
        -}}

- name: gather helm charts
  command: helm list --output=json --all
  register: helm_list
  environment: *kubeconfig

- name: declare helm releases
  set_fact:
    helm_release_names: >-
      {{- (helm_list.stdout | default("{}"))
      | from_json
      | select(attribute="Releases")
      | map(attribute="Name")
      | list
      -}}

- name: render values template
  tempfile:
    state: directory
    suffix: temp
  register: tmp_dir

- name: render the chart values file
  template:
    src: '{{ template_name }}'
    dest: '{{ tmp_dir.path }}/{{ template_name }}'
  register: chart_file
  vars:
    template_name: '{{ chart_name }}-values.{{ chart_version }}.yml'

- name: show the rendered chart and wait if necessary
  when: ansible_verbosity > 3
  block:
  - command: cat {{ chart_file.dest }}
  - pause:
      prompt: look ok?
      seconds: 30

- name: install januschart, if required
  when: janusgraph_helm_name not in helm_release_names
  command: >-
    helm install
    {{ '--debug' if ansible_verbosity > 2 else '' }}
    {{ '--dry-run' if ansible_check_mode else '' }}
    --wait
    --namespace {{ janusgraph_helm_ns }}
    --name {{ janusgraph_helm_name }}
    --timeout 450
    --values {{ chart_file.dest }}
    {{ lookup('fileglob', 'files/janusgraph/Chart.yaml') | dirname }}
  environment: *kubeconfig

- name: remove the rendered values file
  file:
    path: '{{ chart_file.dest }}'
    state: absent
