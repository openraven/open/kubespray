#! /usr/bin/env bash
set -euo pipefail
[[ "${DEBUG:-}" ]] && set -x

container_name="${container_name:-or-ce}"
image_repo="${image_repo:-docker.openraven.io}"
image_name="${image_name:-open/kubespray}"
image_tag="${image_tag:-$RELEASE_ID}"
if [[ -z "${docker_run_image:-}" ]]; then
    docker_run_image="${image_repo}/${image_name}:${image_tag}"
fi
local_port="${local_port:-8000}"
if [[ -z "${entrypoint:-}" ]]; then
    entrypoint='/kubespray/scripts/install_httpd.py'
fi

do_docker_run() {
    # expose any $ANSIBLE_ that are in the environment
    set -- $(env | awk -F= '/^ANSIBLE/{print "-e "$1}') "$@"
    if [[ "${DEBUG:-}" ]]; then
        set -- -e DEBUG "$@"
    fi
    # TODO ask permission for these?
    if [[ "${SSH_AUTH_SOCK:-}" ]]; then
        set -- -e SSH_AUTH_SOCK -v "$SSH_AUTH_SOCK:$SSH_AUTH_SOCK:ro" "$@"
    fi
    exec docker run "$@"
}

# be careful, don't use "--rm" or we will nuke the .tfstate
# and any associated credentials
do_docker_run --name "$container_name" --tty \
    -e DOCKER_HOST \
    -e GROUP_ID \
    -e IN_DOCKER=1 \
    -p ${local_port}:8000 \
    --entrypoint="$entrypoint" \
    "$docker_run_image" \
    "$@"
exit $?
