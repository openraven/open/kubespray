#! /usr/bin/env python3
# coding=utf-8
import itertools
import json
import os
import re
import sys

from boto3.session import Session

SKELETON = {
    "_meta": {
        "hostvars": {},
    },
    "bastion": {
        "hosts": [],
    },
    "etcd": {
        "hosts": [],
    },
    "all": {
        "vars": {},
    },
    "k8s-cluster": {
        "children": [
            "kube-master",
            "kube-node",
        ],
    },
    "kube-master": {
        "hosts": [],
    },
    "kube-node": {
        "hosts": [],
    },
}

AMI_OWNER_TO_USERNAME = {
    '099720109477': 'ubuntu',
    # OpenRaven images use "core"
    '160390125595': 'core',
    '595879546273': 'core',
}


def get_elb_hostname_and_port(elb, cluster_name):
    lb_name = 'orvn-{}-k8s'.format(cluster_name)
    # regrettably this does not support Filters=
    desc_lb = elb.describe_load_balancers(LoadBalancerNames=[lb_name])
    load_balancers = desc_lb['LoadBalancerDescriptions']
    if not load_balancers:
        print('Unable to discover a kubernetes ELB for cluster "{}"'
              .format(cluster_name), file=sys.stderr)
        return None
    elif len(load_balancers) != 1:
        print('WARNING, found MULTIPLE kubernetes ELB for cluster "{}"'
              .format(cluster_name), file=sys.stderr)
    elb_object = load_balancers[0]
    elb_hostname = elb_object['DNSName']
    elb_listeners = elb_object.get('ListenerDescriptions')
    if not elb_listeners:
        print('It seems ELB "{}" has no listeners'.format(lb_name),
              file=sys.stderr)
        return None
    elb_port = elb_listeners[0].get('Listener', {}).get('LoadBalancerPort')
    if not elb_port:
        print('Unable to find a "LoadBalancerPort" for "{}"'.format(lb_name),
              file=sys.stderr)
        return None
    return elb_hostname, elb_port


def get_tagged_cluster_instances(ec2, cluster_name):
    k8s_io_cluster_tag_filter = {
        "Name": "tag-key",
        "Values": [
            "kubernetes.io/cluster/" + cluster_name,
        ],
    }
    # regrettably, the Bastion is tagged just as "Cluster=${cluster_name}"
    cluster_name_filter = {
        "Name": "tag:Cluster",
        "Values": [
            cluster_name,
        ],
    }

    def get_and_flatten(extra_filter):
        inst_response = ec2.describe_instances(
            Filters=[
                {
                    "Name": "instance-state-name",
                    "Values": ["running"]
                },
                extra_filter,
            ],
        )
        reservations = inst_response.get('Reservations', [])
        cluster_instances = [it.get('Instances', []) for it in reservations]
        return list(itertools.starmap(lambda x: x, cluster_instances))

    k8s_io_instances = get_and_flatten(k8s_io_cluster_tag_filter)
    cluster_name_instances = get_and_flatten(cluster_name_filter)
    return k8s_io_instances + cluster_name_instances


def main():
    extra_vars_file = "cluster.json"

    cluster_vars = {}
    if os.path.exists(extra_vars_file):
        print('Reading defaults from "{}"'.format(extra_vars_file),
              file=sys.stderr)
        with open(extra_vars_file) as fh:
            cluster_vars = json.load(fh)
    region_name = cluster_vars.get("aws_default_region")
    cluster_name = cluster_vars.get("aws_kubernetes_cluster_id")
    cluster_ssh_username = cluster_vars.get('ansible_user')
    cluster_ssh_key_file = cluster_vars.get('ansible_ssh_private_key_file')
    del cluster_vars, extra_vars_file
    region_name = os.getenv('AWS_DEFAULT_REGION', region_name)
    if not region_name:
        print('Not without $AWS_DEFAULT_REGION', file=sys.stderr)
        return 1
    cluster_name = os.getenv('CLUSTER_NAME', cluster_name)
    if not cluster_name:
        print('Not without $CLUSTER_NAME', file=sys.stderr)
        return 1

    aws_session = Session(region_name=region_name)
    ec2 = aws_session.client('ec2')
    elb = aws_session.client('elb')

    # cache any AMI lookup to avoid repeatedly asking
    image_owner_by_iid = {}

    result = SKELETON
    hostvars_by_hostname = result["_meta"]["hostvars"]

    bastion_hostnames = result.get('bastion', {}).get('hosts', [])
    etcd_hostnames = result.get('etcd', {}).get('hosts', [])
    kube_master_hostnames = result.get('kube-master', {}).get('hosts', [])
    kube_node_hostnames = result.get('kube-node', {}).get('hosts', [])

    maybe_elb = get_elb_hostname_and_port(elb, cluster_name)
    if not maybe_elb:
        return 1
    elb_hostname, elb_port = maybe_elb
    del maybe_elb

    cluster_instances = get_tagged_cluster_instances(ec2, cluster_name)

    all_vars = {
        'apiserver_loadbalancer_domain_name': elb_hostname,
        'aws_kubernetes_cluster_id': cluster_name,
        'cloud_provider': 'aws',
        'kube_apiserver_port': elb_port,
        'loadbalancer_api_port': elb_port,
    }
    if cluster_ssh_key_file:
        all_vars['ansible_ssh_private_key_file'] = cluster_ssh_key_file
    result['all']['vars'].update(all_vars)

    for inst in cluster_instances:
        # a requirement from --cloud-provider=aws
        inventory_hostname = inst.get('PrivateDnsName')

        image_id = inst.get('ImageId')

        if cluster_ssh_username:
            username = cluster_ssh_username
        else:
            if image_id not in image_owner_by_iid:
                desc_images = ec2.describe_images(ImageIds=[image_id])
                image_list = desc_images.get('Images', [])
                if not image_list:
                    print('Unable to find the AMI for Instance "{}"'
                          .format(inventory_hostname), file=sys.stderr)
                    # or perhaps a default username?
                    continue
                elif len(image_list) != 1:
                    print('EGAD, found multiple AMIs for Instance "{}"'
                          .format(inventory_hostname), file=sys.stderr)
                image_owner = image_list[0].get('OwnerId')
                image_owner_by_iid[image_id] = image_owner
            owner_id = image_owner_by_iid[image_id]
            username = AMI_OWNER_TO_USERNAME.get(owner_id)
            if not username:
                print('EGAD, unmapped AMI OwnerId "{}", cannot sniff out username'
                      .format(owner_id), file=sys.stderr)
                continue

        if not inventory_hostname:
            print('Expected "PrivateDnsName" but found None: {}'
                  .format(repr(inst)), file=sys.stderr)
            continue

        hostvars = {
            'ansible_host': inst.get('PrivateIpAddress'),
            'ansible_user': username,
            'aws_instance_id': inst['InstanceId'],
        }
        for t in inst.get('Tags', []):
            tag_key = t.get('Key')  # type: str
            if not tag_key:
                continue
            friendly_tag_key = re.sub(r'[^A-Za-z0-9_]', '_', tag_key.lower())
            tag_value = t.get('Value', '')
            hostvars['ec2_tag_{}'.format(friendly_tag_key)] = tag_value
        hostvars_by_hostname[inventory_hostname] = hostvars
        member_role_tag = hostvars.get('ec2_tag_role', '')
        if member_role_tag == 'member':
            kube_node_hostnames.append(inventory_hostname)
        if member_role_tag == 'master':
            kube_master_hostnames.append(inventory_hostname)
        if member_role_tag == 'etcd':
            etcd_hostnames.append(inventory_hostname)
        if member_role_tag.startswith('bastion-{}'.format(cluster_name)):
            bastion_hostnames.append(inventory_hostname)
            public_ip = inst.get('PublicIpAddress')
            if not public_ip:
                print('EGAD, it seems your bastion host "{}" has no public-ip'
                      .format(inventory_hostname), file=sys.stderr)
                # well, good luck
            else:
                hostvars['ansible_host'] = public_ip
        del hostvars

    if not etcd_hostnames:
        etcd_hostnames += kube_master_hostnames

    json.dump(result, sys.stdout, indent=2)


if __name__ == '__main__':
    sys.exit(main())
