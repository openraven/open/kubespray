#!/usr/bin/env python

from __future__ import print_function

import json
import os
import sys
import time

import boto3
from argparse import ArgumentParser


class SearchEC2Tags(object):

    def __init__(self):
        self.args = self.parse_args()
        self.cluster_name = os.getenv('CLUSTER_NAME')
        if not self.cluster_name:
            print('WARNING: without $CLUSTER_NAME set in the environment, it will find *all* kubernetes clusters'
                  'and it means this inventory cannot be used for bootstrapping; sleeping 5 seconds ...',
                  file=sys.stderr)
            time.sleep(5)

        region = os.getenv('AWS_REGION', os.getenv('AWS_DEFAULT_REGION', os.getenv('REGION')))
        self.ec2 = boto3.resource('ec2', region)

        # Check if VPC_VISIBILITY is set, if not default to private
        self.vpc_visibility = os.getenv('VPC_VISIBILITY', 'private')

        do_list = True  # list by default
        if self.args.list:
            do_list = True
        if self.args.host:
            do_list = False
        if do_list:
            self.search_tags()
        elif self.args.host:
            data = {}
            print(json.dumps(data, indent=2))

    @staticmethod
    def parse_args():
        """
        :rtype: argparse.Namespace
        """
        # Support --list and --host flags. We largely ignore the host one.
        parser = ArgumentParser()
        parser.add_argument('--list', action='store_true', default=False, help='List instances')
        parser.add_argument('--host', action='store_true', help='Get all the variables about a specific instance')
        return parser.parse_args()

    def search_tags(self):
        hosts = {
            '_meta': {
                'hostvars': {
                    # hosts appear here, with a value of a dict() containing their "vars"
                },
            },
            # groups appear here, including "all" and "ungrouped",
            'k8s-cluster': {
                'children': [
                    'kube-master',
                    'kube-node',
                ],
                "vars": {
                    "aws_kubernetes_cluster_id": self.cluster_name,
                    "cloud_provider": "aws",
                }
            }
        }

        ami_owner_to_username = {
            "595879546273": "core",
            "099720109477": "ubuntu",
        }
        role_tag_by_group = {
            "bastion": "bastion-"+self.cluster_name,
            "kube-master": "master",
            "kube-node": "member",
            "etcd": "etcd",
        }
        use_public_ip_for_groups = [
            'bastion',
        ]
        # any instances that do NOT have "kubernetes.io/cluster/" on them
        skip_cluster_tag_for_groups = [
            'bastion',
        ]
        for group, role_value in role_tag_by_group.items():
            hosts[group] = hosts.get(group, [])
            tag_key = "Role"
            tag_value = ["*" + role_value + "*"]

            filters = [
                {'Name': 'tag:' + tag_key, 'Values': tag_value},
                {'Name': 'instance-state-name', 'Values': ['running']},
            ]
            if self.cluster_name and group not in skip_cluster_tag_for_groups:
                filters.append(
                    {'Name': 'tag-key', 'Values': ['kubernetes.io/cluster/' + self.cluster_name]},
                )

            instances = self.ec2.instances.filter(Filters=filters)
            for instance in instances:
                # Suppose default vpc_visibility is private
                dns_name = instance.private_dns_name
                ssh_host = instance.private_ip_address
                if group in use_public_ip_for_groups:
                    ssh_host = instance.public_ip_address
                ansible_host = {
                    'ansible_ssh_host': ssh_host,
                    # for convenience
                    'aws_instance_id': instance.instance_id,
                }
                ami_owner_id = instance.image.owner_id
                ssh_username = ami_owner_to_username.get(ami_owner_id)
                if ssh_username:
                    ansible_host['ansible_user'] = ssh_username
                else:
                    print('WARNING: no username mapping found for AMI "{}", omitting ssh username',
                          ami_owner_id, file=sys.stderr)

                # Override when vpc_visibility actually is public
                if self.vpc_visibility == "public":
                    dns_name = instance.public_dns_name
                    ansible_host['ansible_ssh_host'] = instance.public_ip_address

                # Set when instance actually has node_labels
                node_labels_tag = list(filter(lambda t: t['Key'] == 'kubespray-node-labels', instance.tags))
                if node_labels_tag:
                    ansible_host['node_labels'] = dict(
                        [label.strip().split('=')
                         for label in node_labels_tag[0]['Value'].split(',')])

                hosts[group].append(dns_name)
                hosts['_meta']['hostvars'][dns_name] = ansible_host

        print(json.dumps(hosts, sort_keys=True, indent=2))


if __name__ == '__main__':
    SearchEC2Tags()
