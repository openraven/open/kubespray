terraform {
  required_version = "~> 0.12"
}

provider "aws" {
  region  = var.AWS_DEFAULT_REGION
  version = "~> 2.25"
}

provider "template" {
  version = "~> 2.1"
}

provider "local" {
  version = "~> 1.4"
}

data "aws_availability_zones" "available" {}

data "local_file" "ignition_userdata" {
  filename = "ignition.json"
}

/*
* Calling modules who create the initial AWS VPC / AWS ELB
* and AWS IAM Roles for Kubernetes Deployment
*/

module "aws-vpc" {
  source = "./modules/vpc"

  aws_cluster_name         = var.aws_cluster_name
  aws_vpc_cidr_block       = var.aws_vpc_cidr_block
  aws_avail_zones          = length(var.aws_availability_zones) != 0 ? sort(var.aws_availability_zones) : sort(data.aws_availability_zones.available.names)
  aws_cidr_subnets_private = var.aws_cidr_subnets_private
  aws_cidr_subnets_public  = var.aws_cidr_subnets_public
  default_tags             = var.default_tags
  name_prefix              = var.name_prefix
  name_prefix_short        = var.name_prefix_short
  nat_gw_per_subnet        = var.aws_vpc_nat_gw_per_subnet
}

module "aws-elb" {
  source = "./modules/elb"

  aws_cluster_name      = var.aws_cluster_name
  aws_vpc_id            = module.aws-vpc.aws_vpc_id
  aws_avail_zones       = module.aws-vpc.aws_availability_zones
  aws_subnet_ids_public = module.aws-vpc.aws_subnet_ids_public
  aws_elb_api_port      = var.aws_elb_api_port
  k8s_secure_api_port   = var.k8s_secure_api_port
  name_prefix           = var.name_prefix
  name_prefix_short     = var.name_prefix_short
  default_tags          = var.default_tags
}

module "aws-iam" {
  source = "./modules/iam"

  aws_cluster_name  = var.aws_cluster_name
  name_prefix       = var.name_prefix
  name_prefix_short = var.name_prefix_short
}

/*
* Create Bastion Instances in AWS
*
*/

resource "aws_instance" "bastion-server" {
  ami                         = data.aws_ami.distro.id
  instance_type               = var.aws_bastion_size
  count                       = 1
  associate_public_ip_address = true
  availability_zone           = element(module.aws-vpc.aws_availability_zones, count.index)
  subnet_id                   = element(module.aws-vpc.aws_subnet_ids_public, count.index)

  vpc_security_group_ids = module.aws-vpc.aws_security_group

  key_name = var.AWS_SSH_KEY_NAME

  tags = merge(var.default_tags, map(
    "Name", "${var.name_prefix}-${var.aws_cluster_name}-bastion-${count.index}",
    "Cluster", var.aws_cluster_name,
    "Role", "bastion-${var.aws_cluster_name}-${count.index}"
  ))

  user_data_base64 = data.local_file.ignition_userdata.content_base64
}

/*
* Create K8s Master and worker nodes and etcd instances
*
*/

resource "aws_instance" "k8s-master" {
  ami           = data.aws_ami.distro.id
  instance_type = var.aws_kube_master_size

  count = var.aws_kube_master_num

  availability_zone      = element(module.aws-vpc.aws_availability_zones, count.index)
  subnet_id              = element(module.aws-vpc.aws_subnet_ids_private, count.index)
  vpc_security_group_ids = module.aws-vpc.aws_security_group

  iam_instance_profile = module.aws-iam.kube-master-profile
  key_name             = var.AWS_SSH_KEY_NAME

  tags = merge(var.default_tags, map(
    "Name", "${var.name_prefix}-${var.aws_cluster_name}-master${count.index}",
    "kubernetes.io/cluster/${var.aws_cluster_name}", "member",
    "Role", "master"
  ))

  user_data_base64 = data.local_file.ignition_userdata.content_base64
}

resource "aws_elb_attachment" "attach_master_nodes" {
  count    = var.aws_kube_master_num
  elb      = module.aws-elb.aws_elb_api_id
  instance = element(aws_instance.k8s-master.*.id, count.index)
}

resource "aws_instance" "k8s-etcd" {
  ami           = data.aws_ami.distro.id
  instance_type = var.aws_etcd_size

  count = var.aws_etcd_num

  availability_zone = element(module.aws-vpc.aws_availability_zones, count.index)
  subnet_id         = element(module.aws-vpc.aws_subnet_ids_private, count.index)

  vpc_security_group_ids = module.aws-vpc.aws_security_group

  key_name = var.AWS_SSH_KEY_NAME

  tags = merge(var.default_tags, map(
    "Name", "${var.name_prefix}-${var.aws_cluster_name}-etcd${count.index}",
    "kubernetes.io/cluster/${var.aws_cluster_name}", "member",
    "Role", "etcd"
  ))

  user_data_base64 = data.local_file.ignition_userdata.content_base64
}

module "worker-pool" {
  source                    = "./modules/worker-asg"
  ami                       = data.aws_ami.distro.id
  ami_owner                 = data.aws_ami.distro.owners[0]
  availability_zones        = module.aws-vpc.aws_availability_zones
  aws_cluster_name          = var.aws_cluster_name
  extra_tags                = var.default_tags
  iam_instance_profile_name = module.aws-iam.kube-worker-profile
  instance_type             = var.aws_kube_worker_size
  key_name                  = var.AWS_SSH_KEY_NAME
  load_balancers            = [module.aws-elb.aws_ingress_elb_name]
  name_prefix               = var.name_prefix
  name_prefix_short         = var.name_prefix_short
  security_group_ids        = module.aws-vpc.aws_security_group
  subnet_ids                = module.aws-vpc.aws_subnet_ids_private
  worker_count_per_az       = var.aws_kube_worker_num_per_az
  user_data_b64             = data.local_file.ignition_userdata.content_base64
}
