output "bastion_iid" {
  value = aws_instance.bastion-server.*.id
}

output "bastion_ip" {
  value = join("\n", aws_instance.bastion-server.*.public_ip)
}

output "masters" {
  value = join("\n", aws_instance.k8s-master.*.private_ip)
}

output "masters_iid" {
  value = aws_instance.k8s-master.*.id
}

output "etcd" {
  value = join("\n", aws_instance.k8s-etcd.*.private_ip)
}

output "etcd_iid" {
  value = aws_instance.k8s-etcd.*.id
}

output "asg_names" {
  value = module.worker-pool.asg_names
}

output "aws_elb_api_fqdn" {
  value = "${module.aws-elb.aws_elb_api_fqdn}:${var.aws_elb_api_port}"
}

output "aws_ingress_fqdn" {
  value = module.aws-elb.aws_ingress_elb_hostname
}

output "default_tags" {
  value = var.default_tags
}
