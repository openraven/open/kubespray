#Global Vars

#VPC Vars
aws_vpc_cidr_block = "10.128.0.0/16"

// currently, us-east-1 is the widest, having from "A" to "F"
// one day, figure out how `cidrsubnets()` works and we won't need to hard-code these
aws_cidr_subnets_public = [
  "10.128.0.0/20",
  "10.128.16.0/20",
  "10.128.32.0/20",
  "10.128.48.0/20",
  "10.128.64.0/20",
  "10.128.80.0/20",
]

aws_cidr_subnets_private = [
  "10.128.128.0/20",
  "10.128.144.0/20",
  "10.128.160.0/20",
  "10.128.176.0/20",
  "10.128.192.0/20",
  "10.128.208.0/20",
]

#Bastion Host
aws_bastion_size = "t2.micro"


#Kubernetes Cluster

aws_kube_master_num  = 3
aws_kube_master_size = "t3.medium"

aws_etcd_num  = 3
aws_etcd_size = "t3.medium"

aws_kube_worker_num_per_az = 1
aws_kube_worker_size       = "t3.medium"

#Settings AWS ELB

aws_elb_api_port    = 6443
k8s_secure_api_port = 6443

default_tags = {
  #  Env = "devtest"
  #  Product = "kubernetes"
}
