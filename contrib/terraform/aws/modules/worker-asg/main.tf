/**
We are using a Launch Template, versus a Launch Configuration,
because changes to LCs mandate that they are destroyed, whereas
an LT can just have its version incremented and life goes on.
*/
data "aws_ami" "worker_ami" {
  filter {
    name   = "image-id"
    values = [var.ami]
  }
  owners = [var.ami_owner]
}

resource "aws_launch_template" "workers" {
  name = "${var.name_prefix}-${var.aws_cluster_name}-workers"

  // Launching a new EC2 instance. Status Reason: EBS-optimized instances are not supported for your requested configuration. Please check the documentation for supported configurations. Launching EC2 instance failed.
  ebs_optimized = false
  iam_instance_profile {
    name = var.iam_instance_profile_name
  }
  image_id      = var.ami
  instance_type = var.instance_type
  key_name      = var.key_name


  block_device_mappings {
    device_name = data.aws_ami.worker_ami.root_device_name
    ebs {
      delete_on_termination = true
      encrypted             = "true"
      snapshot_id           = data.aws_ami.worker_ami.root_snapshot_id
      volume_size           = var.volume_size_in_gb
      volume_type           = "gp2"
    }
  }

  user_data = var.user_data_b64

  vpc_security_group_ids = var.security_group_ids
}

resource "aws_autoscaling_group" "workers" {
  name     = "${aws_launch_template.workers.name}-${var.availability_zones[count.index]}"
  count    = length(var.availability_zones)
  min_size = var.worker_count_per_az
  max_size = var.max_workers

  health_check_grace_period = 360

  launch_template {
    name    = aws_launch_template.workers.name
    version = aws_launch_template.workers.latest_version
  }

  load_balancers = var.load_balancers

  termination_policies = [
    "OldestInstance",
  ]

  tags = concat(
    [
      {
        key                 = "Name"
        value               = "${aws_launch_template.workers.name}-${var.availability_zones[count.index]}"
        propagate_at_launch = true
      },
      {
        key                 = "Role",
        value               = "member"
        propagate_at_launch = true
      },
      map(
        "key", "kubernetes.io/cluster/${var.aws_cluster_name}",
        "value", "member",
        "propagate_at_launch", "true",
      ),
      {
        key                 = "k8s.io/cluster-autoscaler/node-template/label/node-role.kubernetes.io/worker"
        value               = "\"\""
        propagate_at_launch = true
      },
      map(
        "key", "k8s.io/cluster-autoscaler/${var.aws_cluster_name}",
        "value", "true",
        "propagate_at_launch", "false",
      ),
      {
        key                 = "k8s.io/cluster-autoscaler/enable",
        value               = "true"
        propagate_at_launch = false
      },
    ],
    length(var.extra_tags) == 0 ? [] : [var.extra_tags],
  )

  vpc_zone_identifier = [
    var.subnet_ids[count.index],
  ]
}
