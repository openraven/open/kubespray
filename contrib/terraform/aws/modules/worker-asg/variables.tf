variable "aws_cluster_name" {
}

variable "ami" {
}

variable "ami_owner" {
}

variable "availability_zones" {
  type = "list"
}

variable "extra_tags" {
  type = "map"
}

variable "iam_instance_profile_name" {
}

variable "instance_type" {
}

variable "key_name" {
}

variable "load_balancers" {
  description = "any load balancers to which these workers should be attached"
  type        = list(string)
  default     = []
}

variable "max_workers" {
  default = "16"
}

variable "name_prefix" {}

variable "name_prefix_short" {}

variable "security_group_ids" {
  type = "list"
}

variable "subnet_ids" {
  type = "list"
}

variable "user_data_b64" {
  default = ""
}

variable "volume_size_in_gb" {
  default = "32"
}

variable "worker_count_per_az" {
}
