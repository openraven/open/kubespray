resource "aws_security_group" "ingress-elb" {
  name        = "${var.name_prefix}-${var.aws_cluster_name}-ingress-elb"
  description = "control access to the openraven-${var.aws_cluster_name} Ingress"

  vpc_id = var.aws_vpc_id

  tags = merge(var.default_tags, map(
    "Name", "${var.name_prefix}-${var.aws_cluster_name}-ingress-elb"
  ))
}

resource "aws_security_group_rule" "aws-allow-80-access" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "TCP"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ingress-elb.id
}

resource "aws_security_group_rule" "aws-allow-443-access" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "TCP"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ingress-elb.id
}

resource "aws_security_group_rule" "aws-allow-ingress-egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "TCP"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ingress-elb.id
}

resource "aws_elb" "ingress-elb" {
  name = "${var.name_prefix_short}-${var.aws_cluster_name}"

  subnets         = var.aws_subnet_ids_public
  security_groups = [aws_security_group.ingress-elb.id]

  listener {
    instance_port     = 80
    instance_protocol = "tcp"
    lb_port           = 80
    lb_protocol       = "tcp"
  }

  listener {
    instance_port     = 443
    instance_protocol = "tcp"
    lb_port           = 443
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:80"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 4000
  connection_draining         = true
  connection_draining_timeout = 180

  tags = merge(var.default_tags, map(
    "Name", "${var.name_prefix}-${var.aws_cluster_name}-ingress",
    "kubernetes.io/cluster/${var.aws_cluster_name}", "owned",
  ))
}
