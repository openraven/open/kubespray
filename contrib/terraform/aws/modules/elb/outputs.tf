output "aws_elb_api_id" {
  value = aws_elb.aws-elb-api.id
}

output "aws_elb_api_fqdn" {
  value = aws_elb.aws-elb-api.dns_name
}

output "aws_ingress_elb_name" {
  value = aws_elb.ingress-elb.name
}

output "aws_ingress_elb_hostname" {
  value = aws_elb.ingress-elb.dns_name
}
