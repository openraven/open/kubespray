variable "aws_cluster_name" {
  description = "Name of Cluster"
}

variable "name_prefix" {}

variable "name_prefix_short" {}
