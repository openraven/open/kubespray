#Add AWS Roles for Kubernetes

data "local_file" "kube-master-role" {
  filename = "${path.module}/../../../../aws_iam/kubernetes-master-role.json"
}

data "local_file" "kube-worker-role" {
  filename = "${path.module}/../../../../aws_iam/kubernetes-minion-role.json"
}

data "local_file" "kube-master-policy" {
  filename = "${path.module}/../../../../aws_iam/kubernetes-master-policy.json"
}

data "local_file" "kube-worker-policy" {
  filename = "${path.module}/../../../../aws_iam/kubernetes-minion-policy.json"
}

resource "aws_iam_role" "kube-master" {
  name = "${var.name_prefix}-${var.aws_cluster_name}-master"

  assume_role_policy = data.local_file.kube-master-role.content
}

resource "aws_iam_role" "kube-worker" {
  name = "${var.name_prefix}-${var.aws_cluster_name}-node"

  assume_role_policy = data.local_file.kube-worker-role.content
}

resource "aws_iam_role_policy_attachment" "readonly-node" {
  role       = aws_iam_role.kube-worker.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

#Add AWS Policies for Kubernetes

resource "aws_iam_role_policy" "kube-master" {
  name = "${var.name_prefix}-${var.aws_cluster_name}-master"
  role = aws_iam_role.kube-master.id

  policy = data.local_file.kube-master-policy.content
}


resource "aws_iam_role_policy" "kube-worker" {
  name = "${var.name_prefix}-${var.aws_cluster_name}-node"
  role = aws_iam_role.kube-worker.id

  policy = data.local_file.kube-worker-policy.content
}

#Create AWS Instance Profiles

resource "aws_iam_instance_profile" "kube-master" {
  name = aws_iam_role.kube-master.name
  role = aws_iam_role.kube-master.name
}

resource "aws_iam_instance_profile" "kube-worker" {
  name = aws_iam_role.kube-worker.name
  role = aws_iam_role.kube-worker.name
}
