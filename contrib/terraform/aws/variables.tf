variable "AWS_SSH_KEY_NAME" {
  description = "Name of the SSH keypair to use in AWS."
}

variable "AWS_DEFAULT_REGION" {
  description = "AWS Region"
}

//General Cluster Settings

variable "name_prefix" {
  description = "the prefix for resources"
  default     = "openraven"
}

variable "name_prefix_short" {
  description = "the prefix for resources, as short as possible"
  default     = "orvn"
}

variable "aws_cluster_name" {
  description = "Name of AWS Cluster"
}

variable "aws_availability_zones" {
  description = "If specified, constrains the AZs to just these, versus all the ones possible in {AWS_DEFAULT_REGION}"
  type        = list(string)
  default     = []
}

//AWS VPC Variables

variable "aws_vpc_nat_gw_per_subnet" {
  description = "allocate a NAT Gateway per Subnet, or only one for all private subnets?"
  type        = bool
  default     = false
}

variable "aws_vpc_cidr_block" {
  description = "CIDR Block for VPC"
}

variable "aws_cidr_subnets_private" {
  description = "CIDR Blocks for private subnets in Availability Zones"
  type        = "list"
}

variable "aws_cidr_subnets_public" {
  description = "CIDR Blocks for public subnets in Availability Zones"
  type        = "list"
}

//AWS EC2 Settings

variable "aws_bastion_size" {
  description = "EC2 Instance Size of Bastion Host"
}

/*
* AWS EC2 Settings
* The number should be divisable by the number of used
* AWS Availability Zones without an remainder.
*/
variable "aws_kube_master_num" {
  description = "Number of Kubernetes Master Nodes"
}

variable "aws_kube_master_size" {
  description = "Instance size of Kube Master Nodes"
}

variable "aws_etcd_num" {
  description = "Number of etcd Nodes"
}

variable "aws_etcd_size" {
  description = "Instance size of etcd Nodes"
}

variable "aws_kube_worker_num_per_az" {
  description = "Number of Kubernetes Worker Nodes per Availability Zone"
}

variable "aws_kube_worker_size" {
  description = "Instance size of Kubernetes Worker Nodes"
}

/*
* AWS ELB Settings
*
*/
variable "aws_elb_api_port" {
  description = "Port for AWS ELB"
}

variable "k8s_secure_api_port" {
  description = "Secure Port of K8S API Server"
}

variable "default_tags" {
  description = "Default tags for all resources"
  type        = "map"
}
